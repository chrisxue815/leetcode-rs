pub fn my_atoi(s: &str) -> i32 {
    let mut result: i32 = 0;
    let mut undefined_result = i32::max_value();
    let mut func = i32::checked_add as fn(i32, i32) -> Option<i32>;
    let mut chars = s.chars();

    let mut ch: char;

    loop {
        ch = match chars.next() {
            Some(v) => v,
            None => return 0,
        };

        if !ch.is_whitespace() {
            break;
        }
    }

    if ch == '+' {
        ch = match chars.next() {
            Some(v) => v,
            None => return 0,
        };
    }
    else if ch == '-' {
        undefined_result = i32::min_value();
        func = i32::checked_sub as fn(i32, i32) -> Option<i32>;

        ch = match chars.next() {
            Some(v) => v,
            None => return 0,
        };
    }

    let mut digit_option = ch.to_digit(10);

    while digit_option != None {
        let digit = digit_option.unwrap();

        if digit >= 10 {
            break;
        }

        let mut result_option = result.checked_mul(10);
        if result_option == None {
            return undefined_result;
        }

        result_option = func(result_option.unwrap(), digit as i32);
        if result_option == None {
            return undefined_result;
        }

        result = result_option.unwrap();

        ch = match chars.next() {
            Some(v) => v,
            None => break,
        };

        digit_option = ch.to_digit(10);
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        assert_eq!(my_atoi("123"), 123);
        assert_eq!(my_atoi("+123"), 123);
        assert_eq!(my_atoi("-123"), -123);
        assert_eq!(my_atoi("  "), 0);
        assert_eq!(my_atoi(""), 0);
        assert_eq!(my_atoi("   +123"), 123);
        assert_eq!(my_atoi("   -123"), -123);
        assert_eq!(my_atoi("123abc"), 123);
        assert_eq!(my_atoi("999999999999"), i32::max_value());
        assert_eq!(my_atoi("-999999999999"), i32::min_value());
        assert_eq!(my_atoi(i32::min_value().to_string().as_ref()), i32::min_value());
    }
}
