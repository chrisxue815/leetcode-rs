pub fn reverse(mut x: i32) -> i32 {
    let mut result: i32 = 0;

    while x != 0 {
        result = match result.checked_mul(10) {
            Some(v) => v,
            None => return 0,
        };

        let remainder = x % 10;

        result = match result.checked_add(remainder) {
            Some(v) => v,
            None => return 0,
        };

        x /= 10;
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        assert_eq!(reverse(123), 321);
        assert_eq!(reverse(-123), -321);
        assert_eq!(reverse(1534236469), 0);
    }
}
