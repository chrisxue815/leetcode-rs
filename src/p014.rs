pub fn longest_common_prefix(strs: Vec<&str>) -> String {
    if strs.len() == 0 {
        return "".to_owned();
    }

    if strs.len() == 1 {
        return strs[0].to_owned();
    }

    let mut result = String::new();
    let mut iters = Vec::with_capacity(strs.len());

    for s in strs {
        iters.push(s.chars());
    }

    loop {
        let mut chars = iters.iter_mut();
        let ch = match chars.next().unwrap().next() {
            Some(v) => v,
            None => return result,
        };

        for iter in chars {
            match iter.next() {
                Some(v) => if v != ch { return result; },
                None => return result,
            };
        }

        result.push(ch);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        assert_eq!(longest_common_prefix(vec!["12345", "12356"]), "123");
        assert_eq!(longest_common_prefix(vec!["12345", "12356", "1256"]), "12");
        assert_eq!(longest_common_prefix(vec![]), "");
        assert_eq!(longest_common_prefix(vec!["123"]), "123");
    }
}
