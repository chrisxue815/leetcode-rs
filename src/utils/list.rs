pub struct List<T> {
    pub head: Option<Box<ListNode<T>>>,
}

pub struct ListNode<T> {
    pub val: T,
    pub next: Option<Box<ListNode<T>>>,
}

impl<T> List<T> {
    pub fn pop_front_node(&mut self) -> Option<Box<ListNode<T>>> {
        self.head.take().map(|mut front_node| {
            if let Some(node) = front_node.next.take() {
                self.head = Some(node);
            }
            front_node
        })
    }
}
