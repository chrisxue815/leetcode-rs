pub fn convert(s: &str, n: usize) -> String {
    let len = s.chars().count();

    if n == 1 || len < n {
        return s.to_owned();
    }

    let mut result = String::with_capacity(len);
    let offset = 2 * n - 2;

    for i in 0..n {
        let mut j = i;
        while j < len {
            result.push(s.chars().nth(j).unwrap());

            if i != 0 && i != n - 1 {
                let middle = j + 2 * (n - i - 1);
                if middle < len {
                    result.push(s.chars().nth(middle).unwrap());
                }
            }

            j += offset;
        }
    }

    return result;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        assert_eq!(convert("PAYPALISHIRING", 3), "PAHNAPLSIIGYIR");
    }
}
