pub fn is_valid(s: &str) -> bool {
    let mut stack = Vec::new();

    for ch in s.chars() {
        match ch {
            '(' => stack.push(')'),
            '[' => stack.push(']'),
            '{' => stack.push('}'),
            ')' | ']' | '}' => {
                match stack.pop() {
                    Some(v) => if v != ch { return false; },
                    None => return false,
                }
            },
            _ => return false,
        }
    }

    stack.is_empty()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        assert_eq!(is_valid("()"), true);
        assert_eq!(is_valid("()[]{}"), true);
        assert_eq!(is_valid("([{}])"), true);
        assert_eq!(is_valid("("), false);
        assert_eq!(is_valid("(]"), false);
        assert_eq!(is_valid("([)]"), false);
    }
}
