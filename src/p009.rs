use p007;

pub fn is_palindrome(x: i32) -> bool {
    x >= 0 && p007::reverse(x) == x
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        assert_eq!(is_palindrome(121), true);
        assert_eq!(is_palindrome(12321), true);
        assert_eq!(is_palindrome(1), true);
        assert_eq!(is_palindrome(11), true);
        assert_eq!(is_palindrome(-121), false);
        assert_eq!(is_palindrome(123), false);
        assert_eq!(is_palindrome(2147483647), false);
    }
}
