macro_rules! map(
    { $($key:expr => $value:expr,)+ } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key, $value);
            )+
            m
        }
     };
);

pub fn roman_to_int(s: &str) -> i32 {
    if s.len() == 0 {
        return 0;
    }

    let numerals = map!{
        'I' => 1,
        'V' => 5,
        'X' => 10,
        'L' => 50,
        'C' => 100,
        'D' => 500,
        'M' => 1000,
    };

    let mut result = 0;
    let mut prev = 0;

    for cur_char in s.chars() {
        let cur = *(numerals.get(&cur_char).unwrap());

        if cur == prev * 5 || cur == prev * 10 {
            result -= prev;
        }
        else {
            result += prev;
        }

        prev = cur;
    }

    result += prev;

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        assert_eq!(roman_to_int("I"), 1);
        assert_eq!(roman_to_int("IV"), 4);
        assert_eq!(roman_to_int("MMMCMXCIX"), 3999);
    }
}
