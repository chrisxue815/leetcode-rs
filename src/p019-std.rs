use std::collections::LinkedList;

pub fn remove_nth_from_end<T>(list: &mut LinkedList<T>, n: i32) {
    let iter = list.iter();

    let right = iter.skip(n-1);

    if let Some(node) = right {
        right = right.next;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
    }
}
